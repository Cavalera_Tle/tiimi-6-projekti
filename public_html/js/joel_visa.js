
$(document).ready(function () {
    let oikein = 0;
    let vaarin = 0;
    
    $(".vastaus").click(function () {
        let vastaus = Number($(this).val());
        let kysymys = $(this).attr("name");

        $("[name=" + kysymys + "]").prop("disabled", true);
        // jos vastaus on oikein
        if (vastaus === 1) {
            oikein++;
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("oikein");
        // jos vastaus on väärin    
        } else {
            vaarin++;
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("vaarin");  
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
            $("[name=" + kysymys + "][value=1]").parent().addClass("oikein");
            
        }
        // vastauksen jälkeen näytetään selitys sekä oikeiden ja väärien vastausten määrä
        $("#" + kysymys + "_selitys").show();
        $("#" + kysymys + "_tulos").show();
        $("#" + kysymys + "_tulos").html("Oikein: " + oikein + " Väärin: " + vaarin);

    });
    // kysymykset lisätään taulukkoon
    kysymykset = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];

    $(".seuraava").click(function () {
        // kysymys esille
        if (kysymykset.length > 0) {
            let kysymys = getRndInteger(0, kysymykset.length - 1);
            let esille = kysymykset[kysymys];
            $(esille).show();
            kysymykset.splice(kysymys, 1);
            $(this).parent().hide();
        } else {
            // kun kysymykset päättyvät, näytetään loppusivu ja piilotetaan kysymyssivu
            $(".kysymykset").addClass("invisible");
            $(".tulos").show();

            $("#pisteet").html(oikein + "/10" + "<br>");
            
            // kommentit visan päätteeksi riippuen menestyksestä
            if (oikein < 1) {
                $("#pisteet").append("Hupsista, nolla oikein! Ei muuta kuin opiskelemaan eläimiä ja uutta yritystä.");
                return;
            } else if (oikein < 6) {
                $("#pisteet").append("Hieman parannettavaa jäi, mutta ei hätää, sillä harjoitus tekee mestarin.");
                return;
            } else if (oikein < 10) {
                $("#pisteet").append("Hienosti tiedetty! Pienellä kertauksella opit varmasti loputkin.");
                return;
            } else {
                $("#pisteet").append("Täydellistä! Tunnistit kaikki eläimet.");
                return;
            }
        }

    });
    // satunnaislukugeneraattorin koodi
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    // nappi, josta voi aloittaa visan uudelleen pelin päätyttyä
    $("#reset").click(function () {
        location.reload();
    });
});
 
 



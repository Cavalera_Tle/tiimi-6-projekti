/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* Tomi Lepistö */
/* TIK19SP */

$(document).ready(function () {
    
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    let piste1 = [
        '<span><img src="img/Jump (2).png" class="img-fluid" alt=""/></span>'
    ];
    let piste2 = [
        '<span><img src="img/Dead (1).png" class="img-fluid" alt=""/></span>'
    ];
    let tulokset = 0;
    let väärät = 0;
    $(".vastaus").click(function () {

        let vastaus = Number($(this).val());

        let kysymys = $(this).attr("name");

        $("[name=" + kysymys + "]").prop("disabled", true);


        $("#" + kysymys + "_selitys").show();


        if (vastaus === 1) {
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("oikein");
            tulokset++;
            $("#lopputulos").append(piste1 + " ");
        } else {
            väärät++;
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("vaarin");
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
            $("#lopputulos").append(piste2 + " ");
        }
    });

    kyssärit = ["#kys1", "#kys2", "#kys3", "#kys4", "#kys5", "#kys6", "#kys7", "#kys8", "#kys9", "#kys10"];

    $(".seuraava_visa").click(function () {
        if (kyssärit.length > 0) {
            let kysymys = getRndInteger(0, kyssärit.length - 1);
            let esille = kyssärit[kysymys];
            $(esille).show();
            kyssärit.splice(kysymys, 1);
            $(this).parent().hide();
        } else {

            if (tulokset === 10)
                ;
            {
                $(this).parent().hide();

                $("#loppu").removeClass("hided");
                $("#tulos").html(tulokset);
                $("#lopputulos").html();
                if (tulokset >= 8) {
                    $("#loppu").removeClass("hided");
                    $("#tulos").html(tulokset + "<br>" + "Olet varsinainen tietopankki!");
                    $("#lopputulos").html();
                } else if (tulokset >= 6) {
                    $("#loppu").removeClass("hided");
                    $("#tulos").html(tulokset + "<br>" + "Hiukan on vielä petrattavaa, mutta olet selkeästi ollut koulussa hereillä ;)");
                } else if (tulokset >= 3) {
                    $("#loppu").removeClass("hided");
                    $("#tulos").html(tulokset + "<br>" + "Nyt ei mennyt ihan nappiin");
                    $("#lopputulos").html();
                } else {
                    $("#loppu").removeClass("hided");
                    $("#tulos").html(tulokset + "<br>" + "Nyt olisi hyvä aika hankkia tietosanakirjat iltalukemiseksi");
                    $("#lopputulos").html();

                }
            }
        }
    });
    $("#reset").click(function () {
        location.reload();

    });

});
 
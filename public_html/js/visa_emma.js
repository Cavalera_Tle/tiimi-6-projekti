$(document).ready(function () {
    let tulokset = 0;
    let oikea = 0;
    let vaara = 0;

    $(".vastaus").click(function () {
        
        let vastaus = Number($(this).val());

        let kysymys = $(this).attr("name");
      
        $("[name=" + kysymys + "]").prop("disabled", true);

        
        $("#" + kysymys + "_selitys").show();


        if (vastaus === 1) {
            oikea++;
          
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("oikein");
            tulokset++;
        } else {
            vaara++;
        

          
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("vaarin");
            
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
        }
    });

    kysymykset = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];

    $(".seuraava").click(function () {
        if (kysymykset.length > 0) {
            let kysymys = getRndInteger(0, kysymykset.length - 1);
            let esille = kysymykset[kysymys];
            $(esille).show();
            kysymykset.splice(kysymys, 1);
            $(this).parent().hide();
        } else {
            $(".kysymykset").addClass("invisible");
            $(".tulokset").show();

            $("#pisteet").html("<span>" + oikea + "</span>" + "/10");

        }

    });

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    $(".uudestaan").click(function(){
       location.reload();
    });

});
 
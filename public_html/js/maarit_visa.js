/* 
 *Author: Maarit Kokko
 */

$(document).ready(function () {

    tahti = [
        '<span><img src="http://clipart-library.com/images/6Tp5ez8nc.jpg" width="40" height="35" /></span>'
    ];


    let oikea = 0;
    let vaara = 0;


    $(".vastaus").click(function () {
        // lue value-attribuutin arvo muuttujaan, numeerisena
        let vastaus = Number($(this).val());

        let kysymys = $(this).attr("name");
        // $([name=xxx])
        $("[name=" + kysymys + "]").prop("disabled", true);

        // $("#xxx_explanation")
        $("#" + kysymys + "_selitys").show();



        if (vastaus === 1) {
            oikea++;
            // $(this).next().addClass("oikein");

            //lihavoidaan taustaväriä parent-elementin avulla
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("oikein");
            //Oikeista vastauksista saa palkinnon
            $("#star").append(tahti + "");
        } else {
            //  $(this).next().addClass("vaarin")

            //
            vaara++;
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("vaarin");
            // haetaan oikea vastaus ja lihavoidaan seuraavan elementin teksti
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
        }
    });

    kysymykset = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];

    $(".seuraava").click(function () {
        if (kysymykset.length > 0) {
            let kysymys = getRndInteger(0, kysymykset.length - 1);
            let esille = kysymykset[kysymys];
            $(esille).show();
            kysymykset.splice(kysymys, 1);
            // painikkeen vanhempi piilotetaan
            $(this).parent().hide();
        } else {

            $(".visa_tulokset").show();
            $(".visan").addClass("invisible");
            $("#tulos").html("<span>" + oikea + "</span>");
            $("#oikea").html("<span>" + oikea + "</span>");
            $("#vaara").html("<span>" + vaara + "</span>");
            
            //Kun visa loppuu, näkyy tulokset
            if (oikea <= 2) {
                $("#arvosana").html("<span>" + "4. Mutta älä lannistu, harjoitus tekee mestarin!" + "</span>");
            } else if (oikea === 3) {
                $("#arvosana").html("<span>" + "5. Vielä on varaa parantaa!" + "</span>");
            } else if (oikea <= 5) {
                $("#arvosana").html("<span>" + "6. Harjoittelepas vielä lisää! " + "</span>");
            } else if (oikea === 6) {
                $("#arvosana").html("<span>" + "7. Ei huono, mutta parantamisen varaa jäi! " + "</span>");
            } else if (oikea === 7) {
                $("#arvosana").html("<span>" + "8. Hienoa! Sinulla alkaa olla ruotsin kielen alkeet hallussa." + "</span>");
            } else if (oikea <= 8) {
                $("#arvosana").html("<span>" + "9. Vau, sait kiitettävän arvosanan! Hienoa!" + "</span>");
            } else if (oikea === 10) {
                $("#arvosana").html("<span>" + "10. Onneksi olkoon, sait täydet pisteet! Huippusuoritus!" + "</span>");
            }

        }


    });

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

});
 
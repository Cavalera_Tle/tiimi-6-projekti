
$(document).ready(function () {

    let oikein = 0;
    let vaarin = 0;
    let pisteet = 0;
    kuvaoikein = ['<span><img src="img/emmaO.png"/></span>'];
    kuvavaarin = [ '<span><img src="img/emmaV.png"/></span>' ];

    $(".seuraava").click(function () {

        $(".jatka").prop("disabled", true);
        $(".tarkista").prop("disabled", false);
        $(".matikka").addClass("invisible");
        $(".tehtava").show();

        let kokonaisluku = getRndInteger(100, 999);

        $("#kokonaisluku1").html("<span id='kokonaisluku'>" + kokonaisluku + "</span>");


    });

    $(".tarkista").click(function () {

        $(".jatka").prop("disabled", false);


        let kokonaisluku = Number($("#kokonaisluku").html());

        let sadat = Number($("#sadat").val());
        let kymmenet = Number($("#kymmenet").val());
        let ykkoset = Number($("#ykkoset").val());
        let jakojaannos = kokonaisluku % 10;


        kokonaisluku = kokonaisluku - jakojaannos;

        let jakojaannoskymppi = kokonaisluku % 100;

        kokonaisluku = kokonaisluku - jakojaannoskymppi;

        let jakojaannossata = kokonaisluku % 1000;


//tarkistetaan input-elementit, menikö luvut oikein
        if (sadat === jakojaannossata && kymmenet === jakojaannoskymppi &&
                ykkoset === jakojaannos) {
            oikein++;
            pisteet++;
            $("#tarkistus").html("<span>" + "Hyvä! Vastaus on oikein!" + 
                    "<br>" + kuvaoikein + "</span>");

        } else {
            vaarin++;
            pisteet++;
            $("#tarkistus").html("<span>" + "Väärin meni, mutta älä luovuta!" + "<br>" +
                    kuvavaarin + "</span>");
        }
        
        if(pisteet === 10){
            $(".tulokset").show();
            $(".tehtava").hide();
            $(".matikka").hide();
            
            $("#pisteet").html("<span>" + oikein + "/" + pisteet + "</span>");
        }


    });

//tehtävän jatkuminen
    $(".jatka").click(function () {

        $("#ykkoset").val("");
        $("#kymmenet").val("");
        $("#sadat").val("");
        $("#tarkistus").html("");

        $(".jatka").prop("disabled", true);
        $(".tarkista").prop("disabled", false);

        let kokonaisluku = getRndInteger(100, 999);

        $("#kokonaisluku1").html("<span id='kokonaisluku'>" + kokonaisluku + "</span>");


    });
    
    $(".uudestaan").click(function(){
       location.reload();
    });

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
});


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    let tulokset = 0;
    let kaikki = 0;
    $(".vastaus").click(function () {
        
        let vastaus = Number($(this).val());
        let kysymys = $(this).attr("name");
        
        $("[name=" + kysymys + "]").prop("disabled", true);
        $("#" + kysymys + "_selitys").show();


        if (vastaus === 1) {
            //lihavoidaan taustaväriä parent-elementin avulla
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("oikein");
            tulokset++;
            kaikki++;
        } else {
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("vaarin");
            // haetaan oikea vastaus ja lihavoidaan seuraavan elementin teksti
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
            kaikki++;
        }
    });

    // Kysymysten määrä
    kysymykset = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];
    
    //Näyttää seuraavan kysymyksen ja vähentää niitä kunnes määrä on 0
    $(".seuraava").click(function () {
        if (kysymykset.length > 0) {
            let kysymys = getRndInteger(0, kysymykset.length - 1);
            let esille = kysymykset[kysymys];
            $(esille).show();
            kysymykset.splice(kysymys, 1);
            // painikkeen vanhempi piilotetaan
            $(this).parent().hide();    
        }
        //Kysymysten loputtua nappi "Seuraava" nappi piilotetaan, ja tulokset tulevat näkyviin
        else {          
                $(this).hide();
                $(".seuraava").hide();
                $("#loppu").removeClass("hided");
                $(this).hide;
                $("#visanpiilotus").hide();
                
            if (tulokset >+ 8) {
                $("#lopputulos").html(tulokset + "/" + kaikki +  "<br>" +  "Hienosti meni! Olet todellinen tavutusmestari!");
            }
            else if (tulokset >+ 5) {
                $("#lopputulos").html(tulokset + "/" + kaikki +  "<br>" +  "Loistava suoritus!");
            }
           
            else if (tulokset > 3) {
                $("#lopputulos").html(tulokset + "/" + kaikki +  "<br>" +  "Ei aivan nappiin, hyvä yritys kuitenkin!");
            }
                else {
                    $("#lopputulos").html(tulokset + "/" + kaikki +  "<br>" + "Voi räkä! Nyt meni hieman penkin alle. Haluatko yrittää uudelleen?");
                }
            


        }

    });   
    
    // Lataa sivun uudelleen
         $("#uusiyritys3").click(function () {
        location.reload();

    });

    //Arpoo kysymykset satunnaisessa järjestyksessä
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

});


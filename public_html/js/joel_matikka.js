
$(document).ready(function () {
    let oikein = 0;
    let vaarin = 0;
    let luku1 = 0;
    let luku2 = 0;
    let oikea_vastaus = 0;
    let maara = 0;
    
    let kuva1 = [
            '<span><img src="img/kuva1.png" class="img-fluid" alt="kuvake1"/></span>'
        ];
    let kuva2 = [
            '<span><img src="img/kuva2.png" class="img-fluid" alt="kuvake2"/></span>'
        ];
        
    $(".seuraava").click(function(){
       $(".matikka").addClass("invisible");
       $(".lasku").show();
       $(".tarkista").prop("disabled", false);
       $(".seuraava").prop("disabled", true);
       $("#vastaus").val("");
       $("#vastaus").select();
       $("#kommentti").html("");
       $("#palaute").html("");
       
       
        luku1 = getRndInteger(10, 14);
        luku2 = getRndInteger(4, 9);
        oikea_vastaus = luku1 - luku2;
                
        $("#luku1").html(luku1);
        $("#luku2").html(luku2);
        
        if (maara === 10) {
            $(".lasku").addClass("invisible");
            $(".tulos").show();
            $("#pisteet").html(oikein + "/10" + "<br>");
            
            if (oikein < 1) {
                $("#pisteet").append("Sait " + oikein + " oikein. Suunta ei ole kuin ylöspäin!");
                return;
            } else if (oikein < 6) {
                $("#pisteet").append("Sait " + oikein + " oikein. Hyvin laskettu!");
                return;
            } else if (oikein < 10) {
                $("#pisteet").append("Sait " + oikein + " oikein. Loistavaa!");
                return;
            } else {
                $("#pisteet").append("Sait " + oikein + " oikein. Olet oikea matikkanero!");
                return;
            }
        }
    });    
    
    $(".tarkista").click(function(){
        $(".tarkista").prop("disabled", true);
        $(".seuraava").prop("disabled", false);
        let vastaus = Number($("#vastaus").val());
        
        if (vastaus === oikea_vastaus) {
            oikein++;
            maara++;
            $("#kommentti").html("Aivan oikein. Mahtavaa!");
            $("#kuvakkeet").append(kuva1 + " ");
        } else {
            vaarin++;
            maara++;
            $("#kommentti").html("Hupsista keikkaa, väärin meni! Oikea vastaus olisi ollut " + oikea_vastaus);
            $("#kuvakkeet").append(kuva2 + " ");
        }
        $("#palaute").html("Oikein: " + oikein + "<br>" + "Väärin: " + vaarin);
        
    });
    
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    $("#reset").click(function () {
        location.reload();
    });
    
});




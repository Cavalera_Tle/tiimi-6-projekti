$(document).ready(function () {

    let oikein = 0;
    let vaarin = 0;
    let pisteet = 0;
    kuvaoikein = ['<span><img src="img/Jesseoikein.png"/></span>'];

    $(".seuraava").click(function () {

        $(".seuraava").prop("disabled", true);
        $(".tarkista").prop("disabled", false);
        $(".matikka").addClass("invisible");
        $(".tehtava").show();

        let Kilometri = getRndInteger(1, 99);

        $("#Kilometri1").html("<span id='Kilometri'>" + Kilometri + "</span>");

    });

    $(".tarkista").click(function () {

        $(".seuraava").prop("disabled", false);


        let Kilometri = Number($("#Kilometri").html());

        let metrit = Number($("#metrit").val());
        let senttimetrit = Number($("#senttimetrit").val());



        let erisenttimetri = Kilometri * 100000;

        let erimetrit = Kilometri * 1000;
        
        if (metrit === erimetrit && senttimetrit === erisenttimetri) {
            oikein++;
            pisteet++;
            $("#tarkistus").html("<span>" + "Oikein!" + 
                    "<br>" + kuvaoikein + "</span>");

        } else {
            vaarin++;
            pisteet++;
            $("#tarkistus").html("<span>" + "Harmi, väärin meni." + "<br>" + "</span>");
        }
        
        if(pisteet === 10){
            $(".tulokset").show();
            $(".tehtava").hide();
            $(".matikka").hide();
            
            $("#pisteet").html("<span>" + oikein + "/" + pisteet + "</span>");
        }
    });
    $(".seuraava").click(function () {
        
        $("#senttimetrit").val("");
        $("#metrit").val("");
        $("#tarkistus").html("");

        $(".seuraava").prop("disabled", true);
        $(".tarkista").prop("disabled", false);

        let Kilometri = getRndInteger(10, 99);

        $("#Kilometri1").html("<span id='Kilometri'>" + Kilometri + "</span>");


    });
    
    $(".uudestaan").click(function(){
       location.reload();
    });

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
});



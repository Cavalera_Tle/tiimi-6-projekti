/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */
/* Tomi Lepistö */
/* TIK19SP */

/*Pisteitä kuvaavat kuva*/
let piste1 = [
    '<span><img class="img-fluid piste" src="img/Jump (2).png" alt=""/></span>'
];
let piste2 = [
    '<span><img class="img-fluid piste" src="img/Dead (1).png" alt=""/></span>'
];

let pisteesi = 0; //Nollaa pisteet
let kysymyksia = 10; //kysymysten määrä
let piste = 1; //pisteet oikeasta vastauksesta
let maksimi = kysymyksia * piste;

function init() {
    //oikeat vastaukset
    sessionStorage.setItem('a1', 'c');
    sessionStorage.setItem('a2', 'd');
    sessionStorage.setItem('a3', 'c');
    sessionStorage.setItem('a4', 'b');
    sessionStorage.setItem('a5', 'd');
    sessionStorage.setItem('a6', 'c');
    sessionStorage.setItem('a7', 'a');
    sessionStorage.setItem('a8', 'b');
    sessionStorage.setItem('a9', 'd');
    sessionStorage.setItem('a10', 'b');

}

$(document).ready(function () {
//Kysymysten piilottaminen
$('.kysymysAlue').hide();
        $('#startti').show();
        
        $('#submit').click(function () {
$('.startti').hide();
$('.kysymysAlue').hide();
        //Ensimmäisen kysymyksen esittäminen      
        $('#k1').show();
        return false;
        
    });
        $('#k1 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k1');
        $('#k2').show();
        return false;
});
        $('#k2 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k2');
        $('#k3').show();
        return false;
});
        $('#k3 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k3');
        $('#k4').show();
        return false;
});
        $('#k4 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k4');
        $('#k5').show();
        return false;
});
        $('#k5 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k5');
        $('#k6').show();
        return false;
});
        $('#k6 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k6');
        $('#k7').show();
        return false;
});
        $('#k7 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k7');
        $('#k8').show();
        return false;
});
        $('#k8 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k8');
        $('#k9').show();
        return false;
});
        $('#k9 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k9');
        $('#k10').show();
        return false;
});
        $('#k10 #submit').click(function () {
$('.kysymysAlue').hide();
        tarkista('k10');
        $('#vastausKenttä').show();
        return false;
});
        });
//vastausten tarkistus
        function tarkista(k) {
        if (k === "k1") {
        let lähetetty = $('input[name=k1]:checked').val();
                if (lähetetty === sessionStorage.a1) {
        swal("Mahtavaa!", "vastauksesi oli oikein", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("Itku!", "Vastasit väärin", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k2") {
        let lähetetty = $('input[name=k2]:checked').val();
                if (lähetetty === sessionStorage.a2) {
        swal("Huippua!", "Vastasit oikein!", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("Harmin paikka!", "Vastauksesi oli väärä", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k3") {
        let lähetetty = $('input[name=k3]:checked').val();
                if (lähetetty === sessionStorage.a3) {
        swal("Hienoa!", "Vastasit jälleen oikein", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("DAMN!", "vastauksesi oli väärin", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k4") {
        let lähetetty = $('input[name=k4]:checked').val();
                if (lähetetty === sessionStorage.a4) {
        if (tarkista === true);
                swal("Huikeaa!", "Olet varsinainen matemaattinen velho", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("Itku!", "Vastasit väärin", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k5") {
        let lähetetty = $('input[name=k5]:checked').val();
                if (lähetetty === sessionStorage.a5) {
        swal("Mahtavaa!", "vastauksesi oli oikein", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("Voi, voi!", "Tuliko hoppu? vai miksi meni väärin?", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k6") {
        let lähetetty = $('input[name=k6]:checked').val();
                if (lähetetty === sessionStorage.a6) {
        swal("Mahtavaa!", "vastauksesi oli oikein", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("DAMN!", "vastauksesi oli väärin", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k7") {
        let lähetetty = $('input[name=k7]:checked').val();
                if (lähetetty === sessionStorage.a7) {
        swal("Huikeaa!", "Olet varsinainen matemaattinen velho", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("O-Ou!", "Väärin meni", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k8") {
        let lähetetty = $('input[name=k8]:checked').val();
                if (lähetetty === sessionStorage.a8) {
        swal("Huippua!", "Jälleen oikea vastaus", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("Voi, voi!", "Tuliko hoppu? vai miksi meni väärin?", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        
        }
        if (k === "k9") {
        let lähetetty = $('input[name=k9]:checked').val();
                if (lähetetty === sessionStorage.a9) {
        swal("Hienoa!", "Vastasit jälleen oikein", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("Itku!", "Vastasit väärin", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        }
        if (k === "k10") {
        let lähetetty = $('input[name=k10]:checked').val();
                if (lähetetty === sessionStorage.a10) {
        swal("Mahtavaa!", "Vastauksesi oli oikein", "success");
                pisteesi++;
                $("#pistenäyttö").append(piste1 + " ");
        } else {
        swal("Harmin paikka!", "Vastauksesi oli väärä", "error");
                $("#pistenäyttö").append(piste2 + " ");
        }
        $('#vastausKenttä').html('<br><br><h1>Sinun kokonais pisteet ovat: ' + pisteesi + ' / 10 </h1><br><br><a href="luokka_5.html">Kokeile uudestaan</a>');
        }
        return false;
                }
                
//event listener
window.addEventListener('load', init, false);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    // VISA
    
    let oikea = 0;
    let väärin = 0;

    palkinto = [
        '<span class="fa fa-smile-o" aria-hidden="true"></span>'

    ];

    eipalkintoa = [
        '<span class="fa fa-frown-o" aria-hidden="true"></span>'
    ];

    $(".vastaus").click(function () {
        // lue value-attribuutin arvo muuttujaan, numeerisena
        let vastaus = Number($(this).val());

        let kysymys = $(this).attr("name");
        $("[name=" + kysymys + "]").prop("disabled", true);
        $("#" + kysymys + "_selitys").show();

        
        // jos vastaus oikein tulostetaan hymynaama, painotetaan oikea vastaus
        if (vastaus === 1) {
            oikea++;
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("oikein");
            $("#palkinnot").append(palkinto + " ");
        
        // jos vastaus väärin tulostetaan surunaama, lisätään vastaus vääräksi ja näytetään oikea vastaus
        } else {
            väärin++;
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("vaarin");
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
            $("#palkinnot").append(eipalkintoa + " ");
        }
    });

    kysymykset = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];

    // käydään läpi kaikki kysymykset
    $(".seuraava").click(function () {
        if (kysymykset.length > 0) {
            let kysymys = getRndInteger(0, kysymykset.length - 1);
            let esille = kysymykset[kysymys];
            $(esille).show();
            kysymykset.splice(kysymys, 1);
            // painikkeen vanhempi piilotetaan
            $(this).parent().hide();
        } else {

            // näytetään visatulokset ja piilotetaan visakysymykset         
            $(".visa_loppu").show();
            $(".visa_elise").addClass("invisible");

            // tulostetaan pisteet
            $("#pisteet").html("<span>" + oikea + "/10" + "</span>");
            $("#oikeat").html("<span>" + oikea + "</span>");
            $("#väärät").html("<span>" + väärin + "</span>");

            // tulostetaan suoritus
            if (oikea === 10) {
                $("#suoritus").html("<span>" + " Mahtavaa kaikki oikein!" + "</span>");
            } else if (oikea >= 8) {
                $("#suoritus").html("<span>" + " Hienosti meni!" + "</span>");
            } else if (oikea >= 6) {
                $("#suoritus").html("<span>" + " Vielä on parannettavaa!" + "</span>");
            } else if (oikea < 6) {
                $("#suoritus").html("<span>" + " Upsis. Vielä pitäisi harjoitella lisää!" + "</span>");
            }

        }


    });

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    // Matikkatehtävä

    kukka = [
        '<span class="joku"><img src="img/Odysseus-black-flower.png" class="img-fluid"></span>'
    ];

    let luku1 = 0;
    let luku2 = 0;

    let oikeavastaus = 0;
    let vaaravastaus = 0;
    let tehtavat = 0;
    let teht = 1;

    
    // ALOITUS painike
    
    $(".matikkaAloitusEL").click(function () {

        //otetaan jatka painike pois käytöstä
        $(".matikkaJatkaEL").prop("disabled", true);

        // näytetään matikkatehtävä
        $(".matikkaT").show();

        // piilotetaan aloitusteksti ja aloituspainike
        $(".aloitusteksti").removeClass("visible");
        $(".aloitusteksti").addClass("invisible");
        $(".matikkaAloitusEL").hide();
        $(".matikka").hide();



        // arvotaan randomgeneraattorilla luvut laskuun
        luku1 = getRndInteger(1, 10);
        luku2 = getRndInteger(1, 10);

        // tulostetaan luvut
        $("#luku1").html("<span>" + luku1 + "</span");
        $("#luku2").html("<span>" + luku2 + "</span");
        $("#tehtavat").html("<span>" + teht + "</span");

    });
    
    
    // TARKISTA painike
    
    $(".matikkaTarkistaEL").click(function () {

        // otetaan jatkapainike käyttöön ja poistetaan tarkista painike käytöstä
        $(".matikkaJatkaEL").prop("disabled", false);
        $(".matikkaTarkistaEL").prop("disabled", true);
        
        // otetaan value input luvusta ja lasketaan tulos
        let vastaus = Number($("#vastaus").val());
        let tulos = Number(luku1 * luku2 * 1000);

        // tarkistetaan onko vastaus oikein (syötetty input luku), annetaan kukka palkinto
        if (vastaus === tulos) {
            $("#oikeinko").html("<span>" + "OIKEIN! Hienoa, sait kukan palkinnoksi :)" + "</span");
            $("#palkinto").append(kukka + " ");
            oikeavastaus++;
            tehtavat++;
        // Vastaus väärin, tulostetaan oikea vastaus
        } else {
            $("#oikeinko").html("<span>" + "VÄÄRIN! Oikea vastaus olisi ollut " + tulos + "." + "</span");
            vaaravastaus++;
            tehtavat++;
        }
        
        // Kunt tehtäviä on ollut 10, näytetään tulokset ja otetaan buttonit pois käytöstä
        if (tehtavat === 10) {
            $("#oikeat_vastaukset").html("<span>" + "Oikeat vastaukset " + oikeavastaus + "/10" + "</span");
            $(".matikkaTarkistaEL").prop("disabled", true);
            $(".matikkaJatkaEL").prop("disabled", true);
            
            // tulostetaan arvostelu suorituksesta
            if (oikeavastaus === 10) {
                $("#arvostelu").html("Mahtavaa, olet oikea neropatti! Sait kaikki kukat kerättyä!");
            }
            else if (oikeavastaus >= 7) {
                $("#arvostelu").html("Hienosti meni, sait melkein kaikki kukat.");
            }
            else if (oikeavastaus >= 4) {
                $("#arvostelu").html("Aika hyvin, mutta harjoittele vielä lisää.");
            }
            else {
                $("#arvostelu").html("Nyt ei mennyt ihan nappiin. Ensi kerralla paremmin!");
            }
        }

    });

    // JATKA painike
    
    $(".matikkaJatkaEL").click(function () {
        
        // lisätään tehtävänumero ja tulostetaan se
        teht++;       
        $("#tehtavat").html("<span>" + teht + "</span");
        
        // otetaan jatka painike pois käytöstä ja tarkista painike käyttöön
        $(".matikkaJatkaEL").prop("disabled", true);
        $(".matikkaTarkistaEL").prop("disabled", false);

        // tyhjennetään input sekä edellisen tehtävän tulos
        $("#vastaus").val("");
        $("#oikeinko").html("");
        
        // arvotaan uudet luvut laskuun
        luku1 = getRndInteger(1, 10);
        luku2 = getRndInteger(1, 10);
        
        // tulostetaan luvut sivulle
        $("#luku1").html("<span>" + luku1 + "</span");
        $("#luku2").html("<span>" + luku2 + "</span");

    });
    
    // kohdistetaan ja valitaan input luku
    $("#vastaus").focusin(function () {
        $(this).select();
    });


});

 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Koodia lainattu jonkin verran osoitteesta https://stackoverflow.com/questions/28695154/try-to-create-basic-math-game
//Alerteissa oleva "swal" on sweetalert-niminen jQuery-laajennus

$(document).ready(function () {
    //Random-funktion kaava
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }


    //Kuvatiedosto, joka ilmestyy, kun vastaa oikein
    let tahti1 = [
        '<span class="tahti1" ><img src="img/tähti1.png" class="img-fluid" alt=""/></span>'
    ];


    //Piilottaa miinus-laskujen napin ja aloitustekstin
    $("#nappi1").click(function () {
        $("#button1").removeClass("hided");
        $(this).hide();
        $("#nappi2").hide();
        $(this).hide();
        $(".aloitus1").hide();
    });
    //Piilottaa plus-laskujen napin ja aloitustekstin
    $("#nappi2").click(function () {        
        $("#button2").removeClass("hided");
        $(this).hide();
        $("#nappi1").hide();
        $(this).hide();
        $(".aloitus1").hide();
    });
    
    

    //Arpoo numerot plus-lasku funktioon
    let numero1 = getRndInteger(1, 10);
    let numero2 = getRndInteger(1, 10);
    $("#num1").html(numero1);
    $("#num2").html(numero2);

    //Plus-laskun vastaus
    let vastaus = numero1 + numero2;

    //Arpoo numerot miinus-lasku funktioon
    let numero3 = getRndInteger(6, 10);
    let numero4 = getRndInteger(1, 5);
    $("#num3").html(numero3);
    $("#num4").html(numero4);

    //Miinus-laskun vastaus
    let vastaus2 = numero3 - numero4;

    //Kerää oikeat vastaukset "tulokset"-kohtaan ja oikeat sekä väärät "kaikki_vastaukset"-kohtaan
    let tulokset = 0;
    let kaikki_vastaukset = 0;
    
    
    //Plus-laskun kaava
    $("#calculate1").click(function () {
        
        let tarkista = Number($("#vastaus").val());
        if (Number($("#vastaus").val()) === vastaus)
        {
            swal("Mahtavaa!", "Ansaitsit juuri kultaisen tähden!", "success");
            $("#result").append(tahti1);
            tulokset++;
            kaikki_vastaukset++;
        } else {
            swal("Voi ei!", "Vastasit väärin, oikea vastaus on " + vastaus, "error");
            kaikki_vastaukset++;
        }

        //Tyhjentää plus-laskun kaavan ja arpoo uudet luvut seuraavaan laskuun
        Number($("#vastaus").val(""));
        $("#num1").html("");
        $("#num2").html("");
        numero1 = getRndInteger(1, 10);
        numero2 = getRndInteger(1, 10);
        $("#num1").html(numero1);
        $("#num2").html(numero2);

        vastaus = numero1 + numero2;
        
      
      //Kun 10 kysymykseen on vastattu oikein, niin lopputulos tulostuu ja "Tarkista" nappi piilottuu
        if (tulokset === 10) {
            $("#uusiyritys").removeClass("hided");
            $("#loppulaskut").removeClass("hided");
            $(this).hide();
            $("#button1").hide();

        }
        
        if (kaikki_vastaukset === 10) {
            $("#lopputulos1").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Täydellinen suoritus! Olet oikea matikkavelho!");
        }
        else if (kaikki_vastaukset <= 13) {
            $("#lopputulos1").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Lähes täydellinen suoritus, hyvä!");
        }
        else if (kaikki_vastaukset <= 16) {
            $("#lopputulos1").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Ei aivan nappisuoritus, mutta hyvä yritys kuitenkin!");
        }
        else {
            $("#lopputulos1").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Nyt tuli aika monta väärää vastausta. Ehkäpä haluat yrittää uudelleen?");
        }
        
      //Lataa sivun uudelleen "Yritä uudelleen" painikkeesta
     $("#uusiyritys").click(function () {
        location.reload();
    });
    



    });


    //Miinus-laskun kaava
    $("#calculate2").click(function () {

        let tarkista2 = Number($("#vastaus2").val());
        if (Number($("#vastaus2").val()) === vastaus2)
        {
            swal("Mahtavaa!", "Ansaitsit juuri kultaisen tähden!", "success");
            $("#result2").append(tahti1);
            tulokset++;
            kaikki_vastaukset++;
        } else {
            swal("Voi ei!", "Vastasit väärin, oikea vastaus on " + vastaus2, "error");
            kaikki_vastaukset++;
        }

        //Tyhjentää miinuslaskun kaavan ja arpoo uudet luvut seuraavaan laskuun
        Number($("#vastaus2").val(""));
        $("#num3").html("");
        $("#num4").html("");
        numero3 = getRndInteger(6, 10);
        numero4 = getRndInteger(1, 5);
        $("#num3").html(numero3);
        $("#num4").html(numero4);

        vastaus2 = numero3 - numero4;


        //Kun 10 kysymykseen on vastattu oikein, niin lopputulos tulostuu ja "Tarkista" nappi piilottuu
        if (tulokset === 10) {
            $("#loppulaskut2").removeClass("hided");
            $("#lopputulos2").html(tulokset + "/" + kaikki_vastaukset);
            $(this).hide();
            $("#button2").hide();

        }
        if (kaikki_vastaukset === 10) {
            $("#lopputulos2").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Täydellinen suoritus! Olet oikea matikkavelho!");
        }
        else if (kaikki_vastaukset <= 13) {
            $("#lopputulos2").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Lähes täydellinen suoritus, hyvä!");
        }
        else if (kaikki_vastaukset <= 16) {
            $("#lopputulos2").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Ei aivan nappisuoritus, mutta hyvä yritys kuitenkin!");
        }
        else {
            $("#lopputulos2").html("Tarvitsit kymmenen (10) tähden saavuttamiseen " + kaikki_vastaukset + " yritystä." + "<br>" +  "Nyt tuli aika monta väärää vastausta. Ehkäpä haluat yrittää uudelleen?");
        }

    });
      //Lataa sivun uudelleen "Yritä uudelleen" painikkeesta
     $("#uusiyritys2").click(function () {
        location.reload();

    });



});

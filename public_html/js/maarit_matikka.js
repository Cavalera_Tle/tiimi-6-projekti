/*
 * Author: Maarit Kokko
 */

$(document).ready(function () {
    // kuva oikeille vastauksille
    thumbsup = [
        '<span> <img src="img/comic-1302161_640 (2).png" width="40" height="35" /></span>'
    ];

    let numero1 = 0;
    let numero2 = 0;
    let numero11 = 0;
    let numero22 = 0;
    let oikea = 0;
    let vaara = 0;
    let laskut = 0;
    let yritykset = 0;

    function muodostatehtava() {
        //random-generaattori
        yritykset = 0;
        numero1 = getRndInteger(1, 15);
        numero2 = getRndInteger(1, 10);
        numero11 = getRndInteger(1, 10);
        numero22 = getRndInteger(1, 10);

        let numerot = numero1 + numero11 / 100;
        let numerot1 = numero2 + numero22 / 100;

        //lukujen tulostus
        $("#numero1").html("<span>" + numerot + "</span>");
        $("#numero2").html("<span>" + numerot1 + "</span>");
        laskut++;
        // Edellinen summa poistuu ja tilalle uusi lasku.
        $("#summa").val("");
    }
    // Aloita-buttonin click-funktio
    $(".aloitus").click(function () {

        muodostatehtava();

        $(".peli").addClass("invisible");
        $(".peli2").show();
        //Poistaa tekstit kun siirrytään peliin
        $("#selitysta").addClass("invisible");
        $("#selitysta").removeClass("visible");
    });
    
   // Laskun tarkistus
    $(".tarkista").click(function () {
        yritykset++;
        let summa = Number($("#summa").val());
        let vastaus = numero1 + numero11 / 100 + numero2 + numero22 / 100;
        vastaus = vastaus.toFixed(2);
        
        //Pop-up -ikkuna, kun vastaus oikein 
        if (summa.toFixed(2) === vastaus) {
            swal("Oikein meni, hienoa! :)");
            //Hymiö ilmestyy, kun vastaus oikein
            $("#peukalo").append(thumbsup + "");
            oikea++;
            muodostatehtava();
         } else {
            if (yritykset === 1) {
                //Pop-up -ikkuna, kun vastaus väärin.
                swal("Väärä vastaus, yritä uudelleen!");
                vaara++;
            } else {
                //Kerrotaan oikea vastaus ja tulee uusi tehtävä
                swal("Väärin, oikea vastaus on " + vastaus + " ");
                muodostatehtava();
            }
        }

        // Kahdeksan laskun jälkeen näkyy kuinka monta meni oikein.
        if (laskut === 8) {
            
            $("#pisteet").html("<span>" + "Sait oikein " + oikea + " laskua" + "</span>");
            $(".tarkista").prop("disabled", true);

        // Pelin loputtua loppusanat
        if (oikea <= 2) {
            $("#tekstia").html("<span>" + "Ei mennyt ihan putkeen.. tarkkuutta lisää!" + "</span>");
        } else if (oikea <= 4) {
            $("#tekstia").html("<span>" + "Ihan hyvin meni, mutta vielä voi parantaa!" + "</span>");
        } else if (oikea <= 7) {
            $("#tekstia").html("<span>" + "Menipäs se hyvin, melkein täydet pisteet! Jee!" + "</span>");
        } else if (oikea === 8) {
            $("#tekstia").html("<span>" + "Hienoa, sait täydet pisteet!! :)" + "</span>");
        }
    }
     });
   

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
});
